package com.assignment.paytm.backend

import android.util.ArrayMap
import android.util.Log
import com.assignment.paytm.backend.model.DataView
import com.assignment.paytm.backend.model.DisplayView
import com.google.gson.Gson

val jsonToParse = "{\n" +
        "  \"View\": [\n" +
        "    {\n" +
        "      \"dataViews\": [\n" +
        "        {\n" +
        "          \"imageUrl\": \"https://lh3.googleusercontent.com/I_hH8gHVIdMtMGOTMsq6z5fxSwsTUbY0uX8YXm_QaCKB7_s7r-JZUhl1yJgtYU22P44\",\n" +
        "          \"linkClick\": \"click\",\n" +
        "          \"subTitle\": \"First Carousal description\",\n" +
        "          \"title\": \"First Carousal\"\n" +
        "        },\n" +
        "        {\n" +
        "          \"imageUrl\": \"https://beebom.com/wp-content/uploads/2020/01/paytm-all-in-one-QR-code-launched.jpg\",\n" +
        "          \"linkClick\": \"click\",\n" +
        "          \"subTitle\": \"Second Carousal description\",\n" +
        "          \"title\": \"Second Carousal\"\n" +
        "        },\n" +
        "        {\n" +
        "          \"imageUrl\": \"https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQkYB8lZDrVXKQKBSTjlt5hyoQrQBqxc6MkYqKJRrEkXitX_Iyp&usqp=CAU\",\n" +
        "          \"linkClick\": \"click\",\n" +
        "          \"subTitle\": \"Third Carousal description\",\n" +
        "          \"title\": \"Third Carousal\"\n" +
        "        }\n" +
        "      ],\n" +
        "      \"viewType\": \"x\"\n" +
        "    },\n" +
        "    {\n" +
        "      \"dataViews\": [\n" +
        "        {\n" +
        "          \"imageUrl\": \"https://www.flaticon.com/premium-icon/icons/svg/2178/2178632.svg\",\n" +
        "          \"linkClick\": \"click\",\n" +
        "          \"subTitle\": \"Get CashBack\",\n" +
        "          \"title\": \"Cashback & Offers\"\n" +
        "        },\n" +
        "        {\n" +
        "          \"imageUrl\": \"https://www.flaticon.com/premium-icon/icons/svg/2783/2783952.svg\",\n" +
        "          \"linkClick\": \"click\",\n" +
        "          \"subTitle\": \"Refere to friend and earn money\",\n" +
        "          \"title\": \"Refer & Earn\"\n" +
        "        },\n" +
        "        {\n" +
        "          \"imageUrl\": \"https://www.flaticon.com/premium-icon/icons/svg/1940/1940440.svg\",\n" +
        "          \"linkClick\": \"click\",\n" +
        "          \"subTitle\": \"Recharge your device now\",\n" +
        "          \"title\": \"Recharge\"\n" +
        "        },\n" +
        "        {\n" +
        "          \"imageUrl\": \"https://www.flaticon.com/premium-icon/icons/svg/2102/2102262.svg\",\n" +
        "          \"linkClick\": \"click\",\n" +
        "          \"subTitle\": \"Play Games\",\n" +
        "          \"title\": \"Games\"\n" +
        "        }\n" +
        "      ],\n" +
        "      \"viewType\": \"y\"\n" +
        "    },\n" +
        "    {\n" +
        "      \"dataViews\": [\n" +
        "        {\n" +
        "          \"imageUrl\": \"https://www.flaticon.com/premium-icon/icons/svg/1834/1834686.svg\",\n" +
        "          \"linkClick\": \"click\",\n" +
        "          \"subTitle\": \"Recharge prepaid/postpaid\",\n" +
        "          \"title\": \"Recharge\"\n" +
        "        },\n" +
        "        {\n" +
        "          \"imageUrl\": \"https://www.flaticon.com/premium-icon/icons/svg/1866/1866643.svg\",\n" +
        "          \"linkClick\": \"click\",\n" +
        "          \"subTitle\": \"Recharge DTH\",\n" +
        "          \"title\": \"DTH\"\n" +
        "        },\n" +
        "        {\n" +
        "          \"imageUrl\": \"https://www.flaticon.com/premium-icon/icons/svg/2846/2846401.svg\",\n" +
        "          \"linkClick\": \"click\",\n" +
        "          \"subTitle\": \"Information available on Covid 19\",\n" +
        "          \"title\": \"Covid-19\"\n" +
        "        },\n" +
        "        {\n" +
        "          \"imageUrl\": \"https://www.flaticon.com/premium-icon/icons/svg/452/452305.svg\",\n" +
        "          \"linkClick\": \"click\",\n" +
        "          \"subTitle\": \"Elecricity Bill\",\n" +
        "          \"title\": \"Electricity\"\n" +
        "        },\n" +
        "        {\n" +
        "          \"imageUrl\": \"https://www.flaticon.com/premium-icon/icons/svg/2926/2926428.svg\",\n" +
        "          \"linkClick\": \"click\",\n" +
        "          \"subTitle\": \"Best Ludo Game\",\n" +
        "          \"title\": \"Ludo\"\n" +
        "        },\n" +
        "        {\n" +
        "          \"imageUrl\": \"https://www.flaticon.com/premium-icon/icons/svg/2327/2327386.svg\",\n" +
        "          \"linkClick\": \"click\",\n" +
        "          \"subTitle\": \"Get latest updates\",\n" +
        "          \"title\": \"News\"\n" +
        "        },\n" +
        "        {\n" +
        "          \"imageUrl\": \"https://www.flaticon.com/premium-icon/icons/svg/2696/2696165.svg\",\n" +
        "          \"linkClick\": \"click\",\n" +
        "          \"subTitle\": \"Insurance Helper\",\n" +
        "          \"title\": \"Insurance\"\n" +
        "        },\n" +
        "        {\n" +
        "          \"imageUrl\": \"https://www.flaticon.com/premium-icon/icons/svg/2474/2474124.svg\",\n" +
        "          \"linkClick\": \"click\",\n" +
        "          \"subTitle\": \"Check Pension Status\",\n" +
        "          \"title\": \"Pension\"\n" +
        "        },\n" +
        "        {\n" +
        "          \"imageUrl\": \"https://www.flaticon.com/premium-icon/icons/svg/2829/2829008.svg\",\n" +
        "          \"linkClick\": \"click\",\n" +
        "          \"subTitle\": \"Get latest programs\",\n" +
        "          \"title\": \"Programs\"\n" +
        "        }\n" +
        "      ],\n" +
        "      \"viewType\": \"z\"\n" +
        "    }\n" +
        "  ]\n" +
        "}"

fun createJson() {
    val carousalListView: ArrayList<DataView> = ArrayList()

    val carousalDataView1 = DataView(
        "First Carousal",
        "First Carousal description",
        "https://www.fugenx.com/wp-content/uploads/2018/12/How-Much-Does-it-Cost-to-Develop-an-App-like-Paytm.jpg",
        "click"
    )
    val carousalDataView2 = DataView(
        "Second Carousal",
        "Second Carousal description",
        "image",
        "https://beebom.com/wp-content/uploads/2020/01/paytm-all-in-one-QR-code-launched.jpg"
    )
    val carousalDataView3 = DataView(
        "Third Carousal",
        "Third Carousal description",
        "image",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQkYB8lZDrVXKQKBSTjlt5hyoQrQBqxc6MkYqKJRrEkXitX_Iyp&usqp=CAU"
    )

    carousalListView.add(carousalDataView1)
    carousalListView.add(carousalDataView2)
    carousalListView.add(carousalDataView3)

    val gridListView: ArrayList<DataView> = ArrayList()

    val gridDataView1 = DataView(
        "Cashback & Offers",
        "Get CashBack",
        "https://image.flaticon.com/icons/svg/2878/2878577.svg",
        "click"
    )
    val gridDataView2 = DataView(
        "Refer & Earn",
        "Refere to friend and earn money",
        "https://image.flaticon.com/icons/svg/2867/2867902.svg",
        "click"
    )
    val gridDataView3 = DataView(
        "Recharge",
        "Recharge your device now",
        "https://image.flaticon.com/icons/svg/2005/2005816.svg",
        "click"
    )
    val gridDataView4 = DataView(
        "Games",
        "Play Games",
        "https://image.flaticon.com/icons/svg/2917/2917786.svg",
        "click"
    )

    gridListView.add(gridDataView1)
    gridListView.add(gridDataView2)
    gridListView.add(gridDataView3)
    gridListView.add(gridDataView4)


    val normalListView: ArrayList<DataView> = ArrayList()

    val normalListView1 =
        DataView("Recharge", "", "https://image.flaticon.com/icons/svg/2005/2005816.svg", "click")
    val normalListView2 = DataView(
        "DTH",
        "",
        "https://www.flaticon.com/premium-icon/icons/svg/1866/1866643.svg",
        "click"
    )
    val normalListView3 = DataView(
        "Covid-19",
        "Information",
        "https://image.flaticon.com/icons/svg/2913/2913584.svg",
        "click"
    )
    val normalListView4 = DataView(
        "Electricity",
        "",
        "https://www.flaticon.com/premium-icon/icons/svg/2816/2816776.svg",
        "click"
    )
    val normalListView5 = DataView(
        "Ludo",
        "Best Ludo",
        "https://image.flaticon.com/icons/svg/2855/2855570.svg",
        "click"
    )
    val normalListView6 =
        DataView("News", "", "https://image.flaticon.com/icons/svg/2885/2885537.svg", "click")
    val normalListView7 =
        DataView("Insurance", "", "https://image.flaticon.com/icons/svg/2927/2927389.svg", "click")
    val normalListView8 = DataView(
        "Pension",
        "",
        "https://www.flaticon.com/free-icon/retirement-plan_2721217?term=Pension&page=1&position=13",
        "click"
    )
    val normalListView9 =
        DataView("Programs", "", "https://image.flaticon.com/icons/svg/2905/2905983.svg", "click")

    normalListView.add(normalListView1)
    normalListView.add(normalListView2)
    normalListView.add(normalListView3)
    normalListView.add(normalListView4)
    normalListView.add(normalListView5)
    normalListView.add(normalListView6)
    normalListView.add(normalListView7)
    normalListView.add(normalListView8)
    normalListView.add(normalListView9)


    val displayView1 = DisplayView("x", carousalListView)
    val displayView2 = DisplayView("y", gridListView)
    val displayView3 = DisplayView("z", normalListView)

    val listDisplayView = ArrayList<DisplayView>()
    listDisplayView.add(displayView1)
    listDisplayView.add(displayView2)
    listDisplayView.add(displayView3)

    val hashMap1: ArrayMap<String, List<DisplayView>> = ArrayMap()
    hashMap1["View"] = listDisplayView

    Log.v("TAG", Gson().toJson(hashMap1))


}