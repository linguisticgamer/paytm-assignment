package com.assignment.paytm.backend.model


class DisplayView (
    var viewType:String?,
    var dataViews:List<DataView>?
)