package com.assignment.paytm.ui.activities

import android.app.AlertDialog
import android.os.Bundle
import android.os.Handler
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentActivity
import com.assignment.paytm.R
import com.assignment.paytm.backend.jsonToParse
import com.assignment.paytm.databinding.ActivityMainBinding
import com.assignment.paytm.ui.adapters.MainRecyclerAdapter
import com.assignment.paytm.ui.model.MainViewData
import com.assignment.paytm.ui.model.SubViewData
import org.json.JSONObject


class MainActivity :FragmentActivity(){

    lateinit var mActivityMainBinding: ActivityMainBinding
    lateinit var mMainRecyclerAdapter: MainRecyclerAdapter
    var mListMainViewData:ArrayList<MainViewData> = ArrayList()
    var handler:Handler = Handler()
    var builder: AlertDialog.Builder? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        initUI()
        parseJson(jsonToParse)

    }

    private fun initUI(){
        initRecyclerViewFirstTime()
    }

    private fun initRecyclerViewFirstTime(){
        mMainRecyclerAdapter = MainRecyclerAdapter(mListMainViewData, mMultiRecyclerViewClickInterface)
        mActivityMainBinding.rvHome.adapter = mMainRecyclerAdapter
    }

    private val mMultiRecyclerViewClickInterface:MainRecyclerAdapter.MultiRecyclerViewClickInterface = object:MainRecyclerAdapter.MultiRecyclerViewClickInterface{
        override fun onClickItem(mainPosition: Int, innerPosition: Int) {
            val subViewData = mListMainViewData[mainPosition].list[innerPosition]

            if(builder == null){
                builder = AlertDialog.Builder(this@MainActivity)
            }

            builder!!.setTitle(subViewData.title)
            builder!!.setMessage(subViewData.subTitle)
            builder!!.setPositiveButton("Done") { dialog, _ ->
                dialog.dismiss()
            }
            builder!!.show()

        }
    }


    //This is without external Libraries
    private fun parseJson(jsonString:String) {
        if("" == jsonString)
            return

        handler.post(Runnable {
        val jsonObject = JSONObject(jsonString.trim())
        val keys = jsonObject.keys()

        keys?.forEach { key ->
            if(key == "View"){
                val jsonArray = jsonObject.getJSONArray("View")

                if(jsonArray != null && jsonArray.length() != 0) {

                    var mainViewData: MainViewData?
                    var listSubViewData:ArrayList<SubViewData>? = null

                    for (i in 0 until jsonArray.length()) {

                        val subViewObject:JSONObject = jsonArray.get(i) as JSONObject
                        val subViewKeys = subViewObject.keys()

                        subViewKeys.forEach {
                                subViewKey ->
                            if(subViewKey == "dataViews"){
                                listSubViewData = ArrayList()
                                val subViewJsonArray = subViewObject.getJSONArray(subViewKey)

                                for (k in 0 until subViewJsonArray.length()) {
                                    val jsonObject = subViewJsonArray.get(k) as JSONObject
                                    val title = if(jsonObject.has("title"))jsonObject.getString("title") else ""
                                    val subTitle:String? =  if(jsonObject.has("subTitle")) jsonObject.getString("subTitle") else ""
                                    val linkClick =  if(jsonObject.has("linkClick")) jsonObject.getString("linkClick") else ""
                                    val imageUrl =   if(jsonObject.has("imageUrl"))jsonObject.getString("imageUrl") else ""

                                    val subViewData = SubViewData(imageUrl, linkClick,subTitle, title)
                                    listSubViewData!!.add(subViewData)
                                }


                            }

                        }
                        if(!listSubViewData.isNullOrEmpty() && subViewObject.has("viewType")) {
                            mainViewData = MainViewData(subViewObject.getString("viewType"),listSubViewData!!)
                            mListMainViewData.add(mainViewData)
                        }
                    }
                }

            }
        }
        mActivityMainBinding.rvHome.adapter!!.notifyDataSetChanged()
        })
    }

}