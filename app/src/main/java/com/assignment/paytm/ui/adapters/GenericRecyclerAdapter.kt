package com.assignment.paytm.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.assignment.paytm.ui.callbacks.GenericRecyclerInterface

class GenericRecyclerAdapter<T, LVM : ViewDataBinding>(
    items: ArrayList<T>,
    private var layoutId: Int,
    private var bindingInterface: GenericRecyclerInterface<LVM, T>
) : RecyclerView.Adapter<GenericRecyclerAdapter<T, LVM>.RecyclerViewHolder>() {

    private var items: ArrayList<T>? = items


    inner class RecyclerViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private var binding: LVM = DataBindingUtil.bind<LVM>(view)!!

        fun bindData(model: T, pos: Int) {
            bindingInterface.bindData(binding, model, pos)
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(layoutId, parent, false)

        val viewHolder: RecyclerViewHolder
        viewHolder = RecyclerViewHolder(v)
        return viewHolder
    }


    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        val item = items!![position]
        holder.bindData(item, position)
    }

    override fun getItemCount(): Int {
        return if (items == null) {
            0
        } else items!!.size
    }
}