package com.assignment.paytm.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.assignment.paytm.R
import com.assignment.paytm.databinding.*
import com.assignment.paytm.ui.callbacks.GenericRecyclerInterface
import com.assignment.paytm.ui.decorations.RecyclerViewIndicator
import com.assignment.paytm.ui.model.MainViewData
import com.assignment.paytm.ui.model.SubViewData


class MainRecyclerAdapter(private val mainViewDataList: List<MainViewData>, private val mMultiRecyclerViewClickInterface:MultiRecyclerViewClickInterface)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    interface MultiRecyclerViewClickInterface{
        fun onClickItem(mainPosition: Int, innerPosition: Int)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when(viewType){
            0 -> CarousalHolder(DataBindingUtil.inflate(inflater, R.layout.main_item_carousal, parent, false), mMultiRecyclerViewClickInterface)
            1 -> GridViewHolder(DataBindingUtil.inflate(inflater, R.layout.main_item_grid, parent, false), mMultiRecyclerViewClickInterface)
            else -> NormalListHolder(DataBindingUtil.inflate(inflater, R.layout.main_item_normal, parent, false), mMultiRecyclerViewClickInterface)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(getItemViewType(position)){
            0 -> (holder as CarousalHolder).bind(mainViewDataList[position].list, position)
            1 -> (holder as GridViewHolder).bind(mainViewDataList[position].list, position)
            else -> (holder as NormalListHolder).bind(mainViewDataList[position].list, position)
        }
    }

    override fun getItemCount(): Int = mainViewDataList.size

    override fun getItemViewType(position: Int): Int {
        return when (mainViewDataList[position].type) {
            "x" -> 0
            "y" -> 1
            else -> {
                2
            }
        }

    }

}

//carousal
class CarousalHolder(mainItemCarousalBinding: MainItemCarousalBinding, multiRecyclerViewClickInterface: MainRecyclerAdapter.MultiRecyclerViewClickInterface) : RecyclerView.ViewHolder(mainItemCarousalBinding.root) {
    private val mMainItemCarousalBinding:MainItemCarousalBinding = mainItemCarousalBinding
    private val mMultiRecyclerViewClickInterface: MainRecyclerAdapter.MultiRecyclerViewClickInterface = multiRecyclerViewClickInterface
    private var mainPosition:Int = 0

    fun bind(subViewDataList: ArrayList<SubViewData>, position: Int) {
        mainPosition = position
        if(mMainItemCarousalBinding.rvCarousal.adapter == null){
            val pagerSnapHelper = PagerSnapHelper()
            pagerSnapHelper.attachToRecyclerView(mMainItemCarousalBinding.rvCarousal)
            val genericRecyclerAdapter = GenericRecyclerAdapter(
                subViewDataList,
                R.layout.item_carousal,
                object : GenericRecyclerInterface<ItemCarousalBinding, SubViewData> {
                    override fun bindData(
                        binder: ItemCarousalBinding,
                        model: SubViewData,
                        pos: Int
                    ) {
                        binder.subViewData = model
                        binder.position = pos
                        binder.handler = this@CarousalHolder
                    }

                })

            mMainItemCarousalBinding.rvCarousal.adapter = genericRecyclerAdapter
            val activeColor =  ContextCompat.getColor(mMainItemCarousalBinding.root.context, R.color.colorPrimaryDark);
            val inActiveColor = ContextCompat.getColor(mMainItemCarousalBinding.root.context, R.color.black);
            mMainItemCarousalBinding.rvCarousal.addItemDecoration(RecyclerViewIndicator(activeColor, inActiveColor))
        }
        else{
            mMainItemCarousalBinding.rvCarousal.adapter!!.notifyDataSetChanged()
        }
    }

    fun onClickItem(position: Int){
        mMultiRecyclerViewClickInterface.onClickItem(adapterPosition, position)
    }
}

//2*2
class GridViewHolder(mainItemGridBinding: MainItemGridBinding, multiRecyclerViewClickInterface: MainRecyclerAdapter.MultiRecyclerViewClickInterface) : RecyclerView.ViewHolder(mainItemGridBinding.root) {
    private val mMainItemGridBinding: MainItemGridBinding = mainItemGridBinding
    private var mainPosition:Int = 0
    private val mMultiRecyclerViewClickInterface: MainRecyclerAdapter.MultiRecyclerViewClickInterface = multiRecyclerViewClickInterface

    fun bind(
        subViewDataList: ArrayList<SubViewData>,
        position: Int
    ) {
        mainPosition = position
        if (mMainItemGridBinding.rvGrid.adapter == null) {

            mMainItemGridBinding.rvGrid.layoutManager =
                GridLayoutManager(mMainItemGridBinding.root.context, 2, GridLayoutManager.VERTICAL, false)

            val genericRecyclerAdapter = GenericRecyclerAdapter(
                subViewDataList,
                R.layout.item_grid,
                object : GenericRecyclerInterface<ItemGridBinding, SubViewData> {
                    override fun bindData(
                        binder: ItemGridBinding,
                        model: SubViewData,
                        pos: Int
                    ) {
                        binder.position = pos
                        binder.subViewData = model
                        binder.handler = this@GridViewHolder
                    }
                })

            mMainItemGridBinding.rvGrid.adapter = genericRecyclerAdapter
        }
        else{
            mMainItemGridBinding.rvGrid.adapter!!.notifyDataSetChanged()
        }
    }

    fun onClickItem(position: Int){
        mMultiRecyclerViewClickInterface.onClickItem(adapterPosition, position)
    }
}

//3*3
class NormalListHolder(mainItemNormalBinding: MainItemNormalBinding, multiRecyclerViewClickInterface: MainRecyclerAdapter.MultiRecyclerViewClickInterface) : RecyclerView.ViewHolder(mainItemNormalBinding.root) {

    private val mMainNormalItemBinding:MainItemNormalBinding = mainItemNormalBinding
    private var mainPosition:Int = 0
    private val mMultiRecyclerViewClickInterface: MainRecyclerAdapter.MultiRecyclerViewClickInterface = multiRecyclerViewClickInterface


    fun bind(
        subViewDataList: ArrayList<SubViewData>,
        position: Int
    ) {
        mainPosition = position
        if(mMainNormalItemBinding.rvNormal.adapter == null){
            mMainNormalItemBinding.rvNormal.layoutManager =
                GridLayoutManager(mMainNormalItemBinding.root.context, 3, GridLayoutManager.VERTICAL, false)
            val genericRecyclerAdapter = GenericRecyclerAdapter(
                subViewDataList,
                R.layout.item_normal,
                object : GenericRecyclerInterface<ItemNormalBindingImpl, SubViewData> {
                    override fun bindData(
                        binder: ItemNormalBindingImpl,
                        model: SubViewData,
                        pos: Int
                    ) {
                        binder.subViewData = model
                        binder.handler = this@NormalListHolder
                        binder.position = pos
                    }

                })
            mMainNormalItemBinding.rvNormal.adapter = genericRecyclerAdapter
        }
        else{
            mMainNormalItemBinding.rvNormal.adapter!!.notifyDataSetChanged()
        }
    }

    fun onClickItem(position: Int){
        mMultiRecyclerViewClickInterface.onClickItem(adapterPosition, position)
    }
}